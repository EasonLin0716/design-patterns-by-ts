import Swordsman from "./characters/Swordsman";
import Warlock from "./characters/Warlock";
import Dagger from "./weapons/Dagger";

const swordsman = new Swordsman("Joe");
const warlock = new Warlock("Bryan");

swordsman.attack(warlock);
// Switch Attack Strategy
swordsman.equip(new Dagger());
swordsman.attack(warlock);

import Weapon from "./Weapon";
import MagicAttack from "../abilities/MagicAttack";
import Role from "../characters/Role";
import Attack from "../abilities/Attack";
import Character from "../characters/Character";

export default class BasicSword implements Weapon {
  public readonly name = "Basic Sword";

  public attackStrategy = new MagicAttack();

  public availableRoles = [Role.Warlock];

  public switchAttackStrategy(type: Attack): void {
    this.attackStrategy = type;
  }

  public attack(self: Character, target: Character): void {
    this.attackStrategy.attack(self, target);
  }
}

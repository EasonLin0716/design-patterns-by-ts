import Weapon from "./Weapon";
import StabAttack from "../abilities/StabAttack";
import Attack from "../abilities/Attack";
import Character from "../characters/Character";

export default class Dagger implements Weapon {
  public readonly name = "Dagger";

  public attackStrategy = new StabAttack();

  // any roles can use it
  public availableRoles = [];

  public switchAttackStrategy(type: Attack): void {
    this.attackStrategy = type;
  }

  public attack(self: Character, target: Character): void {
    this.attackStrategy.attack(self, target);
  }
}

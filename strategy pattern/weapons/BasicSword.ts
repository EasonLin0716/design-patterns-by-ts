import Weapon from "./Weapon";
import MeleeAttack from "../abilities/MeleeAttack";
import Role from "../characters/Role";
import Attack from "../abilities/Attack";
import Character from "../characters/Character";

export default class BasicSword implements Weapon {
  public readonly name = "Basic Sword";

  public attackStrategy = new MeleeAttack();

  public availableRoles = [Role.Swordsman, Role.Highwayman];

  public switchAttackStrategy(type: Attack): void {
    this.attackStrategy = type;
  }

  public attack(self: Character, target: Character): void {
    this.attackStrategy.attack(self, target);
  }
}

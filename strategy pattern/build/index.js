"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Swordsman_1 = __importDefault(require("./characters/Swordsman"));
const Warlock_1 = __importDefault(require("./characters/Warlock"));
const Dagger_1 = __importDefault(require("./weapons/Dagger"));
const swordsman = new Swordsman_1.default("Joe");
const warlock = new Warlock_1.default("Bryan");
swordsman.attack(warlock);
// Switch Attack Strategy
swordsman.equip(new Dagger_1.default());
swordsman.attack(warlock);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Weapon {
    switchAttackStrategy(type) {
        this.attackStrategy = type;
    }
    attack(self, target) {
        this.attackStrategy.attack(self, target);
    }
}
exports.default = Weapon;

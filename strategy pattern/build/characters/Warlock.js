"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Role_1 = __importDefault(require("./Role"));
const Character_1 = __importDefault(require("./Character"));
const BasicWand_1 = __importDefault(require("../weapons/BasicWand"));
class Warlock extends Character_1.default {
    constructor(name) {
        super(name, Role_1.default.Warlock, new BasicWand_1.default());
    }
}
exports.default = Warlock;

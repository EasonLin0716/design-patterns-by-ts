"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Character {
    constructor(name, role, weaponRef) {
        this.name = name;
        this.role = role;
        this.weaponRef = weaponRef;
    }
    introduce() {
        console.log(`Hi, I'm ${this.name} the ${this.role}`);
    }
    attack(target) {
        this.weaponRef.attack(this, target);
    }
    // 裝備武器
    equip(weapon) {
        const { availableRoles: roles } = weapon;
        if (roles.length === 0 || roles.indexOf(this.role) !== -1) {
            // 裝備此武器
            console.log(`${this.name} has equipped "${weapon.name}"!`);
            this.weaponRef = weapon;
        }
        else {
            // 不能裝備此武器
            throw new Error(`${this.role} cannot equip ${weapon.name}!`);
        }
    }
}
exports.default = Character;

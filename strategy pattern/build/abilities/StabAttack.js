"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class StabAttack {
    attack(self, target) {
        console.log(`${self.name} stabs through ${target.name} with his dagger!`);
    }
}
exports.default = StabAttack;

import EquipmentFactory from "./EquipmentFactory";
import BasicWand from "../weapons/BasicWand";
import BasicArmour from "../armours/BasicArmour";

class WarlockEquipmentFactory implements EquipmentFactory {
  public createWeapon() {
    return new BasicWand();
  }

  public createArmour() {
    return new BasicArmour();
  }
}

export default WarlockEquipmentFactory;

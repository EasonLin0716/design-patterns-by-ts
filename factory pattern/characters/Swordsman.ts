import Role from "./Role";
import Character from "./Character";
import SwordsmanEquipmentFactory from "../equipments/SwordsmanEquipmentFactory";

export default class Swordsman extends Character {
  constructor(name: string) {
    let SEF = new SwordsmanEquipmentFactory();
    // 藉由工廠幫忙製作武器防具
    super(name, Role.Swordsman, SEF.createWeapon(), SEF.createArmour());
  }
}

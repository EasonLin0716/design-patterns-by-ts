"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Armour_1 = __importDefault(require("./Armour"));
const Role_1 = __importDefault(require("../characters/Role"));
class BasicRobe extends Armour_1.default {
    constructor() {
        super(...arguments);
        this.name = "Basic Robe";
        this.availableRoles = [Role_1.default.Warlock];
    }
}
exports.default = BasicRobe;

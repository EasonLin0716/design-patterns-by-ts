"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Armour_1 = __importDefault(require("./Armour"));
const Role_1 = __importDefault(require("../characters/Role"));
class BasicArmour extends Armour_1.default {
    constructor() {
        super(...arguments);
        this.name = "Basic Armour";
        this.availableRoles = [Role_1.default.Swordsman, Role_1.default.BountyHunter];
    }
}
exports.default = BasicArmour;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Equipments_1 = __importDefault(require("../equipments/Equipments"));
class Armour {
    constructor() {
        this.type = Equipments_1.default.Armour;
    }
}
exports.default = Armour;

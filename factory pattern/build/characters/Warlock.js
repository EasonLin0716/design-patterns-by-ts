"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Role_1 = __importDefault(require("./Role"));
const Character_1 = __importDefault(require("./Character"));
const WarlockEquipmentFactory_1 = __importDefault(require("../equipments/WarlockEquipmentFactory"));
class Warlock extends Character_1.default {
    constructor(name) {
        let WEF = new WarlockEquipmentFactory_1.default();
        super(name, Role_1.default.Warlock, WEF.createWeapon(), WEF.createArmour());
    }
}
exports.default = Warlock;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Role_1 = __importDefault(require("./Role"));
const Character_1 = __importDefault(require("./Character"));
class Swordsman extends Character_1.default {
    constructor(name) {
        super(name, Role_1.default.Swordsman);
    }
}
exports.default = Swordsman;

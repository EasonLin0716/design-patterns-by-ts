"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Weapon_1 = __importDefault(require("../weapons/Weapon"));
const Armour_1 = __importDefault(require("../armours/Armour"));
class Character {
    constructor(name, role, weaponRef, armourRef) {
        this.name = name;
        this.role = role;
        this.weaponRef = weaponRef;
        this.armourRef = armourRef;
    }
    introduce() {
        console.log(`Hi, I'm ${this.name} the ${this.role}`);
    }
    attack(target) {
        this.weaponRef.attack(this, target);
    }
    // 裝備武器
    equip(equipment) {
        const { availableRoles: roles } = equipment;
        if (roles.length === 0 || roles.indexOf(this.role) !== -1) {
            if (equipment instanceof Weapon_1.default) {
                this.weaponRef = equipment;
            }
            else if (equipment instanceof Armour_1.default) {
                this.armourRef = equipment;
            }
            console.log(`${this.name} has equipped "${equipment.name}"!`);
        }
        else {
            // 不能裝備此武器
            throw new Error(`${this.role} cannot equip ${equipment.name}!`);
        }
    }
}
exports.default = Character;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Role_1 = __importDefault(require("./Role"));
const Character_1 = __importDefault(require("./Character"));
const SwordsmanEquipmentFactory_1 = __importDefault(require("../equipments/SwordsmanEquipmentFactory"));
class Swordsman extends Character_1.default {
    constructor(name) {
        let SEF = new SwordsmanEquipmentFactory_1.default();
        // 藉由工廠幫忙製作武器防具
        super(name, Role_1.default.Swordsman, SEF.createWeapon(), SEF.createArmour());
    }
}
exports.default = Swordsman;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MeleeAttack {
    attack(self, target) {
        console.log(`${self.name} strikes ${target.name} with a big sword!`);
    }
}
exports.default = MeleeAttack;

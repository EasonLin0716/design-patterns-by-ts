"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MagicAttack {
    attack(self, target) {
        console.log(`${self.name} casts magic and pierces through ${target.name}!`);
    }
}
exports.default = MagicAttack;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Weapon_1 = __importDefault(require("./Weapon"));
const StabAttack_1 = __importDefault(require("../abilities/StabAttack"));
class Dagger extends Weapon_1.default {
    constructor() {
        super(...arguments);
        this.name = "Dagger";
        this.attackStrategy = new StabAttack_1.default();
        // any roles can use it
        this.availableRoles = [];
    }
    switchAttackStrategy(type) {
        this.attackStrategy = type;
    }
    attack(self, target) {
        this.attackStrategy.attack(self, target);
    }
}
exports.default = Dagger;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Weapons_1 = __importDefault(require("./Weapons"));
const BasicSword_1 = __importDefault(require("./BasicSword"));
const BasicWand_1 = __importDefault(require("./BasicWand"));
const Dagger_1 = __importDefault(require("./Dagger"));
class WeaponFactory {
    createWeapon(type) {
        switch (type) {
            case Weapons_1.default.BasicSword:
                return new BasicSword_1.default();
            case Weapons_1.default.BasicWand:
                return new BasicWand_1.default();
            case Weapons_1.default.Dagger:
                return new Dagger_1.default();
            default:
                throw new Error(`${Weapons_1.default[type]} isn't registered!`);
        }
    }
}
exports.default = WeaponFactory;

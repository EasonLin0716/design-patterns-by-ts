"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Weapon_1 = __importDefault(require("./Weapon"));
const MeleeAttack_1 = __importDefault(require("../abilities/MeleeAttack"));
const Role_1 = __importDefault(require("../characters/Role"));
class BasicSword extends Weapon_1.default {
    constructor() {
        super(...arguments);
        this.name = "Basic Sword";
        this.attackStrategy = new MeleeAttack_1.default();
        this.availableRoles = [Role_1.default.Swordsman, Role_1.default.Highwayman];
    }
    switchAttackStrategy(type) {
        this.attackStrategy = type;
    }
    attack(self, target) {
        this.attackStrategy.attack(self, target);
    }
}
exports.default = BasicSword;

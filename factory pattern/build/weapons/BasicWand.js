"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Weapon_1 = __importDefault(require("./Weapon"));
const MagicAttack_1 = __importDefault(require("../abilities/MagicAttack"));
const Role_1 = __importDefault(require("../characters/Role"));
class BasicWand extends Weapon_1.default {
    constructor() {
        super(...arguments);
        this.name = "Basic Sword";
        this.attackStrategy = new MagicAttack_1.default();
        this.availableRoles = [Role_1.default.Warlock];
    }
    switchAttackStrategy(type) {
        this.attackStrategy = type;
    }
    attack(self, target) {
        this.attackStrategy.attack(self, target);
    }
}
exports.default = BasicWand;

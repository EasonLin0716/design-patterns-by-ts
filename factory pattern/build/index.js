"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Swordsman_1 = __importDefault(require("./characters/Swordsman"));
const Warlock_1 = __importDefault(require("./characters/Warlock"));
const Weapons_1 = __importDefault(require("./weapons/Weapons"));
const WeaponFactory_1 = __importDefault(require("./weapons/WeaponFactory"));
const swordsman = new Swordsman_1.default("Joe");
const warlock = new Warlock_1.default("Bryan");
const weaponFactory = new WeaponFactory_1.default();
swordsman.attack(warlock);
// Switch Attack Strategy
swordsman.equip(weaponFactory.createWeapon(Weapons_1.default.Dagger));
swordsman.attack(warlock);

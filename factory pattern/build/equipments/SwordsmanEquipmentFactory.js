"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BasicSword_1 = __importDefault(require("../weapons/BasicSword"));
const BasicArmour_1 = __importDefault(require("../armours/BasicArmour"));
class SwordsmanEquipmentFactory {
    createWeapon() {
        return new BasicSword_1.default();
    }
    createArmour() {
        return new BasicArmour_1.default();
    }
}
exports.default = SwordsmanEquipmentFactory;

import Swordsman from "./characters/Swordsman";
import Warlock from "./characters/Warlock";
import Weapons from "./weapons/Weapons";
import WeaponFactory from "./weapons/WeaponFactory";

const swordsman = new Swordsman("Joe");
const warlock = new Warlock("Bryan");
const weaponFactory = new WeaponFactory();

swordsman.attack(warlock);
// Switch Attack Strategy
swordsman.equip(weaponFactory.createWeapon(Weapons.Dagger));
swordsman.attack(warlock);
